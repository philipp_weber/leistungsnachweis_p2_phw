package dev.mze.spring.controller;

import dev.mze.spring.model.mitarbeiter.Mitarbeiter;
import dev.mze.spring.model.mitarbeiter.MitarbeiterDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
public class MitarbeiterController {

    @Autowired
    private MitarbeiterDAO mitarbeiterDAO;

    @GetMapping("/mitarbeiter/get-all")
    public List<Mitarbeiter> getallMitarbeiter() {
        return mitarbeiterDAO.getallMitarbeiter();
    }

    @GetMapping("/mitarbeiter/get-single/{id}")
    public ResponseEntity<Mitarbeiter> getsingleMitarbeiter(@PathVariable(value="id") Integer persnr) {
        Optional<Mitarbeiter> m = mitarbeiterDAO.getsingleMitarbeiter(persnr);
        if (m.isPresent()) {
            return ResponseEntity.ok(m.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/mitarbeiter/save")
    public Mitarbeiter save(@RequestBody Mitarbeiter mitarbeiter) {
        return mitarbeiterDAO.save(mitarbeiter);
    }

}
