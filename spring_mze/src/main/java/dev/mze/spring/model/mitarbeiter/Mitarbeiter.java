package dev.mze.spring.model.mitarbeiter;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Mitarbeiter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int persnr;
    private String name;
    private String vorname;
    private String gbdatum;

    public int getPersnr() {
        return persnr;
    }

    public void setPersnr(int persnr) {
        this.persnr = persnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getGbdatum() {
        return gbdatum;
    }

    public void setGbdatum(String gbdatum) {
        this.gbdatum = gbdatum;
    }

    @Override
    public String toString() {
        return "Mitarbeiter{" +
                "persnr=" + persnr +
                ", name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", gbdatum=" + gbdatum +
                '}';
    }
}
