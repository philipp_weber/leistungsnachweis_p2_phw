package dev.mze.spring.model.zeiterfassung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ZeiterfassungDAO {

    @Autowired
    private ZeiterfassungRepository repository;

    public Zeiterfassung save(Zeiterfassung zeiterfassung) {
        return repository.save(zeiterfassung);
    }

    public List<Zeiterfassung> getallZeiterfassung() {
        List<Zeiterfassung> zeiterfassung = new ArrayList<>();
        Streamable.of(repository.findAll())
                .forEach(zeiterfassung::add);
        return zeiterfassung;
    }

    public Optional<Zeiterfassung> getsingleZeiterfassung(Integer zeid) {
        return repository.findById(zeid);
    }


    public void delete(Zeiterfassung zeiterfassung) {
        repository.delete(zeiterfassung);
    }
}
