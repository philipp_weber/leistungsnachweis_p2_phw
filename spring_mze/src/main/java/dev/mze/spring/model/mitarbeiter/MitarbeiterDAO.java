package dev.mze.spring.model.mitarbeiter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MitarbeiterDAO {

    @Autowired
    private MitarbeiterRepository repository;

    public Mitarbeiter save(Mitarbeiter mitarbeiter) {
       return repository.save(mitarbeiter);
    }

    public List<Mitarbeiter> getallMitarbeiter() {
        List<Mitarbeiter> mitarbeiter = new ArrayList<>();
        Streamable.of(repository.findAll())
                .forEach(mitarbeiter::add);
        return mitarbeiter;
    }

    public Optional<Mitarbeiter> getsingleMitarbeiter(Integer persnr) {
        return repository.findById(persnr);
    }

    public void delete(Mitarbeiter mitarbeiter) {
        repository.delete(mitarbeiter);
    }
}
