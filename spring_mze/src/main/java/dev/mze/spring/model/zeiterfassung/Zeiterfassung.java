package dev.mze.spring.model.zeiterfassung;

import dev.mze.spring.model.mitarbeiter.Mitarbeiter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

@Entity
public class Zeiterfassung {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int zeid;
    private String datum;
    private String von;
    private String bis;
    private String taetigkeit;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "mitarbeiter_persnr")
    private Mitarbeiter mitarbeiter;

    public int getZeid() {
        return zeid;
    }

    public void setZeid(int zeid) {
        this.zeid = zeid;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getVon() {
        return von;
    }

    public void setVon(String von) {
        this.von = von;
    }

    public String getBis() {
        return bis;
    }

    public void setBis(String bis) {
        this.bis = bis;
    }

    public String getTaetigkeit() {
        return taetigkeit;
    }

    public void setTaetigkeit(String taetigkeit) {
        this.taetigkeit = taetigkeit;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    @Override
    public String toString() {
        return "Zeiterfassung{" +
                "zeid=" + zeid +
                ", datum=" + datum +
                ", von=" + von +
                ", bis=" + bis +
                ", taetigkeit='" + taetigkeit + '\'' +
                ", mitarbeiter=" + mitarbeiter +
                '}';
    }
}
