package com.example.mitarbeiterzeiterfassung.retrofit;

import com.example.mitarbeiterzeiterfassung.model.Zeiterfassung;
import java.util.List;
import java.util.Optional;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ZeiterfassungAPI {

    @GET("/zeiterfassung/get-all")
    Call<List<Zeiterfassung>> getAllZeiterfassung();
    //
    @GET("/zeiterfassung/get-single/{id}")
    Call<Zeiterfassung> getSingleZeiterfassung(@Path("id") Integer zeid);
    //Call<Optional<Zeiterfassung>> getSingleZeiterfassung(@Body int zeid);
    //
    @POST("/zeiterfassung/save")
    Call<Zeiterfassung> save(@Body Zeiterfassung zeiterfassung);


}
