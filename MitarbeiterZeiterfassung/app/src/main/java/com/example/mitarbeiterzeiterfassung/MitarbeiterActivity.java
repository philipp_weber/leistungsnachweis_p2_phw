package com.example.mitarbeiterzeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.example.mitarbeiterzeiterfassung.model.Mitarbeiter;
import com.example.mitarbeiterzeiterfassung.retrofit.MitarbeiterAPI;
import com.example.mitarbeiterzeiterfassung.retrofit.RetrofitService;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MitarbeiterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mitarbeiter);

        initializeComponents();
    }

    private void initializeComponents() {
        TextInputEditText inputEditVorname = findViewById(R.id.form_textFieldVorname);
        TextInputEditText inputEditNachname = findViewById(R.id.form_textFieldNachname);
        TextInputEditText inputEditGbdatum = findViewById(R.id.form_textFieldGbdatum);

        MaterialButton buttonMSpeichern = findViewById(R.id.form_buttonMSpeichern);

        RetrofitService retrofitService = new RetrofitService();
        MitarbeiterAPI mitarbeiterAPI = retrofitService.getRetrofit().create(MitarbeiterAPI.class);

        buttonMSpeichern.setOnClickListener(view -> {

            String vorname = String.valueOf(inputEditVorname.getText());
            String nachname = String.valueOf(inputEditNachname.getText());
            String gbdatum = String.valueOf(inputEditGbdatum.getText());

            Mitarbeiter mitarbeiter = new Mitarbeiter();
            mitarbeiter.setVorname(vorname);
            mitarbeiter.setName(nachname);
            mitarbeiter.setGbdatum(gbdatum);

            mitarbeiterAPI.save(mitarbeiter).enqueue(new Callback<Mitarbeiter>() {
                @Override
                public void onResponse(Call<Mitarbeiter> call, Response<Mitarbeiter> response) {
                    Toast.makeText(MitarbeiterActivity.this, "Speichern erfolgreich!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Mitarbeiter> call, Throwable t) {
                    Toast.makeText(MitarbeiterActivity.this, "Speichern fehlgeschlagen!", Toast.LENGTH_SHORT).show();
                    Logger.getLogger(MitarbeiterActivity.class.getName()).log(Level.SEVERE, "Fehler aufgetreten");
                }
            });

        });
    }
}