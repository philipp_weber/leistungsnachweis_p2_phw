package com.example.mitarbeiterzeiterfassung.model;

import java.time.LocalDate;

public class Mitarbeiter {

    private int persnr;
    private String name;
    private String vorname;
    private String gbdatum;

    public int getPersnr() {
        return persnr;
    }

    public void setPersnr(int persnr) {
        this.persnr = persnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getGbdatum() {
        return gbdatum;
    }

    public void setGbdatum(String gbdatum) {
        this.gbdatum = gbdatum;
    }

    @Override
    public String toString() {
        return "Mitarbeiter{" +
                "persnr=" + persnr +
                ", name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", gbdatum=" + gbdatum +
                '}';
    }
}
