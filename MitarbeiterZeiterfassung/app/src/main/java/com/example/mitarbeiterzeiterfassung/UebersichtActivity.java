package com.example.mitarbeiterzeiterfassung;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;
import com.example.mitarbeiterzeiterfassung.adapter.ZeiterfassungAdapter;
import com.example.mitarbeiterzeiterfassung.model.Zeiterfassung;
import com.example.mitarbeiterzeiterfassung.retrofit.RetrofitService;
import com.example.mitarbeiterzeiterfassung.retrofit.ZeiterfassungAPI;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UebersichtActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uebersicht);

        recyclerView = findViewById(R.id.uebersicht_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadZeiterfassung();
    }

    private void loadZeiterfassung() {
        RetrofitService retrofitService = new RetrofitService();
        ZeiterfassungAPI zeiterfassungAPI = retrofitService.getRetrofit().create(ZeiterfassungAPI.class);

        zeiterfassungAPI.getAllZeiterfassung()
                .enqueue(new Callback<List<Zeiterfassung>>() {
                    @Override
                    public void onResponse(Call<List<Zeiterfassung>> call, Response<List<Zeiterfassung>> response) {
                        populateListView(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Zeiterfassung>> call, Throwable t) {
                        Toast.makeText(UebersichtActivity.this, "Laden fehlgeschlagen", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void populateListView(List<Zeiterfassung> zeiterfassungList) {
        ZeiterfassungAdapter zeiterfassungAdapter = new ZeiterfassungAdapter(zeiterfassungList);
        recyclerView.setAdapter(zeiterfassungAdapter);

    }
}