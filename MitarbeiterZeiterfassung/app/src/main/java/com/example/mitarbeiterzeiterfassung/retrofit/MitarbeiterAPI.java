package com.example.mitarbeiterzeiterfassung.retrofit;

import com.example.mitarbeiterzeiterfassung.model.Mitarbeiter;
import java.util.List;
import java.util.Optional;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MitarbeiterAPI {

    @GET("/mitarbeiter/get-all")
    Call<List<Mitarbeiter>> getAllMitarbeiter();
    //
    @GET("/mitarbeiter/get-single/{id}")
    Call<Mitarbeiter> getSingleMitarbeiter(@Path("id") Integer persnr);
    //
    @POST("/mitarbeiter/save")
    Call<Mitarbeiter> save(@Body Mitarbeiter mitarbeiter);

}
