package com.example.mitarbeiterzeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.mitarbeiterzeiterfassung.model.Mitarbeiter;
import com.example.mitarbeiterzeiterfassung.model.Zeiterfassung;
import com.example.mitarbeiterzeiterfassung.retrofit.MitarbeiterAPI;
import com.example.mitarbeiterzeiterfassung.retrofit.ZeiterfassungAPI;
import com.example.mitarbeiterzeiterfassung.retrofit.RetrofitService;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ErfassungActivity extends AppCompatActivity {

    private int persnr;
    private Mitarbeiter mitarbeiter;
    TextInputEditText inputEditPersnr;
    TextInputEditText inputEditVorname;
    TextInputEditText inputEditNachname;
    TextInputEditText inputEditGbdatum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_erfassung);

        Intent int_main = getIntent();
        String persnr_text = int_main.getStringExtra(MainActivity.EXTRA_PERSNR);
        persnr = Integer.parseInt(persnr_text);

        initializeComponents();
    }

    private void initializeComponents() {

        inputEditPersnr = findViewById(R.id.form_textFieldPersnr);
        inputEditVorname = findViewById(R.id.form_textFieldVorname);
        inputEditNachname = findViewById(R.id.form_textFieldNachname);
        inputEditGbdatum = findViewById(R.id.form_textFieldGbdatum);
        TextInputEditText inputEditEdatum = findViewById(R.id.form_textFieldEdatum);
        TextInputEditText inputEditVon = findViewById(R.id.form_textFieldVon);
        TextInputEditText inputEditBis = findViewById(R.id.form_textFieldBis);
        TextInputEditText inputEditTaetigkeit = findViewById(R.id.form_textFieldTaetigkeit);

        MaterialButton buttonZESpeichern = findViewById(R.id.form_buttonZESpeichern);

        RetrofitService retrofitService = new RetrofitService();
        MitarbeiterAPI mitarbeiterAPI = retrofitService.getRetrofit().create(MitarbeiterAPI.class);
        ZeiterfassungAPI zeiterfassungAPI = retrofitService.getRetrofit().create(ZeiterfassungAPI.class);

        mitarbeiterAPI.getSingleMitarbeiter(persnr).enqueue(new Callback<Mitarbeiter>() {
            @Override
            public void onResponse(Call<Mitarbeiter> call, Response<Mitarbeiter> response) {
                Toast.makeText(ErfassungActivity.this, "Lesen erfolgreich!", Toast.LENGTH_SHORT).show();
                setMitarbeiter(response.body());
                runOnUiThread(() -> displayMitarbeiter());

            }

            @Override
            public void onFailure(Call<Mitarbeiter> call, Throwable t) {
                Toast.makeText(ErfassungActivity.this, "Lesen fehlgeschlagen!", Toast.LENGTH_SHORT).show();
                Logger.getLogger(ErfassungActivity.class.getName()).log(Level.SEVERE, "Fehler aufgetreten");
            }
        });

        buttonZESpeichern.setOnClickListener(view -> {

            String edatum = String.valueOf(inputEditEdatum.getText());
            String von = String.valueOf(inputEditVon.getText());
            String bis = String.valueOf(inputEditBis.getText());
            String taetigkeit = String.valueOf(inputEditTaetigkeit.getText());

            Zeiterfassung zeiterfassung = new Zeiterfassung();
            zeiterfassung.setDatum(edatum);
            zeiterfassung.setVon(von);
            zeiterfassung.setBis(bis);
            zeiterfassung.setTaetigkeit(taetigkeit);

            zeiterfassung.setMitarbeiter(mitarbeiter);

            zeiterfassungAPI.save(zeiterfassung).enqueue(new Callback<Zeiterfassung>() {
                @Override
                public void onResponse(Call<Zeiterfassung> call, Response<Zeiterfassung> response) {
                    Toast.makeText(ErfassungActivity.this, "Speichern erfolgreich!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Zeiterfassung> call, Throwable t) {
                    Toast.makeText(ErfassungActivity.this, "Speichern fehlgeschlagen!", Toast.LENGTH_SHORT).show();
                    Logger.getLogger(ErfassungActivity.class.getName()).log(Level.SEVERE, "Fehler aufgetreten");
                }
            });
        });
    }

    private void setMitarbeiter(Mitarbeiter ma) {
        mitarbeiter = ma;

    }

    private void displayMitarbeiter() {
        inputEditPersnr.setText(String.valueOf(mitarbeiter.getPersnr()));
        inputEditVorname.setText(mitarbeiter.getVorname());
        inputEditNachname.setText(mitarbeiter.getName());
        inputEditGbdatum.setText(mitarbeiter.getGbdatum());

    }
}