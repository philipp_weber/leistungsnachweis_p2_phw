package com.example.mitarbeiterzeiterfassung.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mitarbeiterzeiterfassung.R;
import com.example.mitarbeiterzeiterfassung.model.Zeiterfassung;
import java.util.List;

public class ZeiterfassungAdapter extends RecyclerView.Adapter<ZeiterfassungHolder> {

    private List<Zeiterfassung> zeiterfassungList;

    public ZeiterfassungAdapter(List<Zeiterfassung> zeiterfassungList) {
        this.zeiterfassungList = zeiterfassungList;
    }

    @NonNull
    @Override
    public ZeiterfassungHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_zeiterfassung_item, parent, false);
        return new ZeiterfassungHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ZeiterfassungHolder holder, int position) {
        Zeiterfassung zeiterfassung = zeiterfassungList.get(position);

        holder.zeid.setText(String.valueOf(zeiterfassung.getZeid()));
        holder.persnr.setText(String.valueOf(zeiterfassung.getMitarbeiter().getPersnr()));
        holder.edatum.setText(zeiterfassung.getDatum());
        holder.von.setText(zeiterfassung.getVon());
        holder.bis.setText(zeiterfassung.getBis());
        holder.taetigkeit.setText(zeiterfassung.getTaetigkeit());
    }

    @Override
    public int getItemCount() {
        return zeiterfassungList.size();
    }
}
