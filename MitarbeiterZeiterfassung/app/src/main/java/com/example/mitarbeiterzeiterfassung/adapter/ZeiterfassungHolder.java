package com.example.mitarbeiterzeiterfassung.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mitarbeiterzeiterfassung.R;


public class ZeiterfassungHolder extends RecyclerView.ViewHolder {

    TextView zeid, persnr, edatum, von, bis, taetigkeit;

    public ZeiterfassungHolder(@NonNull View itemView) {
        super(itemView);
        zeid = itemView.findViewById(R.id.zeiterfassungListItem_zeid);
        persnr = itemView.findViewById(R.id.zeiterfassungListItem_persnr);
        edatum = itemView.findViewById(R.id.zeiterfassungListItem_edatum);
        von = itemView.findViewById(R.id.zeiterfassungListItem_von);
        bis = itemView.findViewById(R.id.zeiterfassungListItem_bis);
        taetigkeit = itemView.findViewById(R.id.zeiterfassungListItem_taetigkeit);
    }
}
