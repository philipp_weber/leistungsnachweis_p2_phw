package com.example.mitarbeiterzeiterfassung;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_PERSNR = "com.example.mitarbeiterzeiterfassung.extra.PERSNR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeComponents();
    }

    private void initializeComponents() {
        TextInputEditText inputEditPersnr = findViewById(R.id.form_textFieldPersnr);
        MaterialButton buttonMitarbeiter = findViewById(R.id.form_buttonMitarbeiter);
        MaterialButton buttonErfassung = findViewById(R.id.form_buttonErfassung);
        MaterialButton buttonUebersicht = findViewById(R.id.form_buttonUebersicht);

        buttonMitarbeiter.setOnClickListener(view -> {
            Intent int_mitarbeiter = new Intent(this, MitarbeiterActivity.class);
            startActivity(int_mitarbeiter);
        });

        buttonErfassung.setOnClickListener(view -> {
            String persnr = String.valueOf(inputEditPersnr.getText());
            if (persnr.isEmpty()) {
                Toast.makeText(MainActivity.this, "Gültige PersNr eingeben", Toast.LENGTH_SHORT).show();
            } else {
                Intent int_erfassung = new Intent(this, ErfassungActivity.class);
                int_erfassung.putExtra(EXTRA_PERSNR, persnr);
                startActivity(int_erfassung);
            }
        });

        buttonUebersicht.setOnClickListener(view -> {
            Intent int_uebersicht = new Intent(this, UebersichtActivity.class);
            startActivity(int_uebersicht);
        });

    }
}