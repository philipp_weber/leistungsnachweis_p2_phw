package com.example.mitarbeiterzeiterfassung.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Zeiterfassung {

    private int zeid;
    private String datum;
    private String von;
    private String bis;
    private String taetigkeit;

    private Mitarbeiter mitarbeiter;

    public int getZeid() {
        return zeid;
    }

    public void setZeid(int zeid) {
        this.zeid = zeid;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getVon() {
        return von;
    }

    public void setVon(String von) {
        this.von = von;
    }

    public String getBis() {
        return bis;
    }

    public void setBis(String bis) {
        this.bis = bis;
    }

    public String getTaetigkeit() {
        return taetigkeit;
    }

    public void setTaetigkeit(String taetigkeit) {
        this.taetigkeit = taetigkeit;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    @Override
    public String toString() {
        return "Zeiterfassung{" +
                "zeid=" + zeid +
                ", datum=" + datum +
                ", von=" + von +
                ", bis=" + bis +
                ", taetigkeit='" + taetigkeit + '\'' +
                ", mitarbeiter=" + mitarbeiter +
                '}';
    }
}
